# SolventNet
This repository contains all python scripts for SolventNet. 

Please see the Zenodo for more details about running the code:
https://zenodo.org/record/4460617#.YDA5XBNKh-V

# File description
- `train_deep_cnn.py`: main code to train deep CNNs
- `publishable_images.py`: main code to print out published images
- `deep_cnn_vox_net.py`: contains the VoxNet model
- `deep_cnn_solvent_net_3.py`: contains the SolventNet model
- `deep_cnn_ORION.py`: contains the ORION model
- `generate_grid_interpolation.py`: main code to run grid interpolation
- `predict_test_set.py`: code to predict the test set
- `visualize_importance_maps.py`: code to analyze importance maps from 3D CNNs