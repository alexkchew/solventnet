#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
analyze_deep_cnn_separated_instances.py
The purpose of this script is to simply generate analysis for 
'extract_deep_cnn_separated_instances.py'. It will load the deep cnn, then 
it will run the analysis flag. 

Written by: Alex K. Chew (03/03/2020)

"""
import os
import numpy as np
## CHECKING TOOLS
from core.check_tools import check_testing

## IMPORTING COMBINING ARRAYS
from combining_arrays import combine_instances
## IMPORTING TRAIN DEEP CNN
from train_deep_cnn import train_deep_cnn
## IMPORTING ANALYSIS
from analyze_deep_cnn import analyze_deep_cnn

## IMPORTING ML FUNCTIONS
from core.ml_funcs import get_list_args

## IMPORTING GLOBAL VARIABLES
from core.global_vars import SOLUTE_TO_TEMP_DICT, CNN_DICT, SAMPLING_DICT

## IMPORTING NOMENCLATURE
from core.nomenclature import read_combined_name, extract_representation_inputs, extract_sampling_inputs

#%% MAIN FUNCTION
if __name__ == "__main__":
    
    ## SEE IF TESTING IS ON
    testing = check_testing()
    
    ## DEFINING DICTIONARY FOR CNN DICT
    cnn_dict = CNN_DICT
    
    ## CHECKING TESTING
    if testing == True:
        ## DEFINING PATH TO INSTANCE 
        output_path = r"/Volumes/akchew/scratch/3d_cnn_project/simulations/MANUSCRIPT_0_TRAINING_3DCNNS_ALLDATA/20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-strlearn-1.00-solvent_net-500-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-10_25_50_75-DIO_GVL_THF"
        # r"/Volumes/akchew/scratch/3d_cnn_project/simulations/MANUSCRIPT_0_TRAINING/20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-strlearn-1.00-solvent_net-500-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-10_25_50_75-DIO_GVL_THF"
        # r"R:\scratch\3d_cnn_project\simulations\20200221-5fold_train_20_20_20_20ns_oxy_3chan\20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-strlearn-0.80-solvent_net-500-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-10_25_50_75-DIO_GVL_THF"
        pickle_name = "20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-DIO_GVL_THF-10_25_50_75" 
        # r"20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-DIO_GVL_THF-10_25_50_75"    
        ## DEFINING NUMBER OF CROSS VALIDATIONS
        num_cross_validation_folds = 0
        # 5
        ## DEFINING INDICES NAMES
        indices_pickle_name="indices.pickle"
        ## DEFINING RESULTS PICKLE NAME
        results_pickle = r"model.results"
        want_training_test_pickle = False
        want_basic_name = True
        ## DEFINING LOGICALS
        want_augment= False
        want_descriptors = False
        retrain = False # No retraining
        verbose = True
        
        ## DEFINING REWRITE
        rewrite = True
        ## SELECTING TYPE OF CNN
        cnn_type = 'solvent_net' # 'orion' # 'solvent_net' 'voxnet'
        ## DEFINING SAMPLING
        sampling_type = 'strlearn'
        sampling_inputs = [1.00] 

        ## DEFINING INDICES PICKLE
        indices_pickle = "indices.pickle"
        # 0.80
        
    else:
        ## ADDING OPTIONS 
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        #####################################
        # ---- Taken from training_cnn ---- #
        #####################################
        ## MODEL TYPE
        parser.add_option("-q", "--cnntype", dest="cnn_type", type=str, help="Type of cnn (e.g. voxnet, solventnet, orion)", default = 'voxnet')
        
        ## NUMBER OF EPOCHS
        parser.add_option("-n", "--epochs", dest="epochs", type=int, help="Number of epochs", default = 500)
        
        ## DIRECTORY LOCATIONS
        parser.add_option('-d', '--database', dest = 'database_path', help = 'Full path to database', default = None)
        parser.add_option('-c', '--classfile', dest = 'class_file_path', help = 'Full path to class csv file', default = None)
        parser.add_option('-a', '--combinedfile', dest = 'combined_database_path', help = 'Full path to combined pickle directory', default = None)
        parser.add_option('-o', '--outputfile', dest = 'output_file_path', help = 'Full path to output weights and pickles', default = None)
        parser.add_option("-v", dest="verbose", action="store_true", )

        ## DEFINING RETRAINING IF NECESSARY
        parser.add_option("-t", dest="retrain", action="store_true", default = False )
        
        ## STORING TRAINING PICKLE
        parser.add_option("-p", dest="want_training_test_pickle", action="store_true", default = False )
        parser.add_option("-b", dest="want_basic_name", action="store_true", default = False )
        
        ## WANT DESCRIPTORS
        parser.add_option("--want_descriptors", dest="want_descriptors", action="store_true", default = False )
        
        ## SAMPLING TYPE
        parser.add_option('--samplingtype', dest = 'sampling_type', help = 'Sampling type', default = 'split_average', type=str)
        parser.add_option("--samplinginputs", dest="sampling_inputs", action="callback", type="string", callback=get_list_args,
                  help="For multiple inputs, simply separate by comma (no whitespace)", default = 5)
        
        ## AUGMENTATION
        parser.add_option("--no_augment", dest="want_augment", action="store_false", default = True ) 
        
        ## CROSS VALIDATION FOLDS
        parser.add_option("--num_cross_folds", dest="num_cross_validation_folds", help = "Number of cross validation folds", type=int, default = 1 ) 
        
        ## DEFINING OUTPUT PICKLE
        parser.add_option('--output_path', dest = 'output_path', help = 'Full path output the pickle file', default = None)

        ## DEFINING OUTPUT PICKLE
        parser.add_option('--pickle_name', dest = 'pickle_name', help = 'Name of the pickle', default = None)       
        
        ## DEFINING OUTPUT PICKLE
        parser.add_option('--indices_pickle', dest = 'indices_pickle', help = 'Name of the output indices pickle', default = '')        
        
        ## DEFINING OUTPUT PICKLE
        parser.add_option('--cross_valid_index', dest = 'cross_valid_index', help = 'Index of cross valididation to run', default = 0)       
        
        # ---- Taken from training_cnn ---- #
        
        ## GETTING RESULTS PICKLE
        parser.add_option('--results_pickle', dest = 'results_pickle', help = 'Results pickle after analysis', default = None)
        
        ## SEEING IF YOU WANT TO REWRITE
        parser.add_option('--rewrite', dest = 'rewrite', help = 'True if you want to rewrite', default = False)
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## DEFINING MODEL TYPE
        cnn_type = options.cnn_type
        
        ## AUGMENTATION
        want_augment = options.want_augment
        
        ## DEFINING SAMPLING INPUTS
        sampling_type = options.sampling_type
        sampling_inputs = options.sampling_inputs
        
        ## DEFINING CROSS VALIDATION FOLDS
        num_cross_validation_folds = options.num_cross_validation_folds
        
        ## VERBOSITY
        verbose = options.verbose
        
        ## RETRAIN
        retrain = options.retrain
        
        ## TRAINING TEST PICKLE
        want_training_test_pickle = options.want_training_test_pickle
        want_basic_name = options.want_basic_name
        want_descriptors = options.want_descriptors
        
        ## STORING OUTPUT PATH
        output_path = options.output_path
        ## DEFINING PICKLE
        pickle_name = options.pickle_name
        
        ## DEFINING INDICES PICKLE
        indices_pickle = options.indices_pickle
        
        ## ADDING EPOCHS
        cnn_dict['epochs'] = options.epochs
        
        ## DEFINING INDEX
        cross_valid_index = options.cross_valid_index

        #########
        ## OUTPUT THE NAMES
        results_pickle = options.results_pickle
        
        ## DEFINING SAMPLING INPUTS
        sampling_type = options.sampling_type
        sampling_inputs = options.sampling_inputs
        
        ## DEFINING CROSS VALIDATION FOLDS
        num_cross_validation_folds = options.num_cross_validation_folds
        
        ## ADDING EPOCHS
        cnn_dict['epochs'] = options.epochs
        
        ## DEFINING LOGICALS
        want_augment = options.want_augment
        
        ## DEFINING REWRITE
        rewrite = options.rewrite
        
        if rewrite == "true":
            rewrite = True
        elif rewrite == "false":
            rewrite = False
        else:
            print("Warning, rewrite set to: %s"%(rewrite))
            print("By default, we set rewrite to be either true or false")
            print("Setting rewrite to false")
            rewrite = False
        
    ## UPDATING SAMPLING INPUTS
    sampling_dict = extract_sampling_inputs( sampling_type = sampling_type, 
                                             sampling_inputs = sampling_inputs,)
    

        
    ## DEFINING RESULTS PATH
    results_file_path = os.path.join(output_path, results_pickle)
    
    ## CHECKING IF RESULTS EXISTS
    if os.path.isfile(results_file_path) is False or rewrite is True:
        
        ## LOADING THE DATA
        instances = combine_instances(
                         output_path = output_path,
                         pickle_name = pickle_name,
                         )
        
        ### TRAINING CNN
        deep_cnn = train_deep_cnn(
                         instances = instances,
                         sampling_dict = sampling_dict,
                         cnn_type = cnn_type,
                         cnn_dict = cnn_dict,
                         retrain=retrain,
                         output_path = output_path,
                         class_file_path = None,
                         verbose = verbose,
                         want_training_test_pickle = want_training_test_pickle,
                         want_basic_name = want_basic_name,
                         want_descriptors = want_descriptors,
                         want_augment = want_augment,
                         num_cross_validation_folds = num_cross_validation_folds,
                         k_fold_indices_pickle = indices_pickle,
                         k_fold_index_selection = None,
                         )
        ### ANALYZING DEEP CNN
        analysis = analyze_deep_cnn( instances = instances, 
                                     deep_cnn = deep_cnn )
        
        ## STORING ANALYSIS
        if testing is False:
            analysis.store_pickle( results_file_path = results_file_path )
            print("Creating results in: %s"%(results_file_path) )
    else:
        print("Results file exist: %s"%( results_file_path ) )

    

    