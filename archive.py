# -*- coding: utf-8 -*-
"""
archive.py
This script contains old code that was not used.
Created on Tue Aug 13 10:11:13 2019

@author: akchew
"""

## MAIN FUNCTION
if __name__ == "__main__":
    
    #%%
    ## FIGURE 1: Reaction kinetics results
    
    ## DEFINING FIGURE SIZE
    # figure_size=( 16.8/2, 16.8/2 )
    # figure_size=( 18.542/2, 18.542/2 )
    figure_size=( 18.542/2, 18.542/3 )
    
    ## DEFINING PATH TO DATA
    path_experimental_data=r"R:\scratch\3d_cnn_project\database\Experimental_Data\solvent_effects_regression_data.csv"
    
    ## READING CSV FILE
    csv_file = pd.read_csv( path_experimental_data )
    
    ## DEFINING SPECIFIC COSOLVENT
    desired_cosolvent = "GVL"
    
    ## DEFINING DATA
    data_of_cosolvent = csv_file.loc[csv_file.cosolvent == desired_cosolvent]
    
    ## RENAMING
    data_of_cosolvent = rename_dataframe_entries(dataframe = data_of_cosolvent)
    
    ## DEFINING FIGURE NAME
    fig_name = "experimental_reaction_rates_%s"%(desired_cosolvent)
    
    ## CREATING FIGURE
    fig, ax = plotter.create_fig_based_on_cm( figure_size )
    
    ## SETTING AXIS LABELS
    ax.set_xlabel("delta")
    ax.set_ylabel("sigma")
    
    ## DEFINING TICKS
    x_ticks = (0, 1.00, 0.2 ) # 0.5
    y_ticks = (-1.5, 2.5, 0.5 ) # 0.5
    
    ## DEFINING XY LIMITS
    x_lims = (-0.1, 1.1)
    # y_lims = (-1.5, 2.5)
    y_lims = (-0.5, 2.5)
    
    ## SETTING X TICKS AND Y TICKS
    ax.set_xticks(np.arange(x_ticks[0], x_ticks[1] + x_ticks[2], x_ticks[2]))
    ax.set_yticks(np.arange(y_ticks[0], y_ticks[1] + y_ticks[2], y_ticks[2]))
    
    ax.set_xlim([x_lims[0], x_lims[1]] )
    ax.set_ylim([y_lims[0], y_lims[1]])
    
    ## LOOPING THROUGH ENTRIES
    for idx, row in data_of_cosolvent.iterrows():
        ## DEFINING VARIABLES
        solute = row['solute']
        ## MASS FRACTION
        mass_frac_water = correct_mass_frac_water( row['mass_frac_water'] )
        ## DEFINING SIGMA
        sigma = row['sigma_label']
        ## DEFINING DELTA
        delta = row['delta']
        ## COSOLVENT
        cosolvent = row['cosolvent']
        
        ## DEFINING MARKER, ETC.
        color = plotter.SOLUTE_COLOR_DICT[solute]
        label = solute
        marker = plotter.MASS_FRAC_SYMBOLS_DICT[mass_frac_water]
        fillstyle = plotter.COSOLVENT_FILL_DICT[cosolvent]                
        
        ## PLOT POINTS WITHOUT ERROR BAR
        ax.plot(delta, 
                sigma, 
                marker = marker, 
                color=color, 
                linewidth=1, 
                fillstyle = fillstyle,
                label = label + '_' + mass_frac_water,
                **plotter.PLOT_INFO) # label = "Max tree depth: %s"%(max_depth)
    
    ## DRAWING LINE AT Y=0
    ax.axhline(y=0, linestyle='--', linewidth=0.5, color='gray')
    
    ## SHOW TIGHT LAYOUT
    fig.tight_layout()
    
    ## STORING FIGURE
    store_figure( fig = fig, 
                  path = os.path.join(path_image_dir, fig_name),
                  save_fig = save_fig,
                  fig_extension = fig_extension,
                 )