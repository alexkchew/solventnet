# -*- coding: utf-8 -*-
"""
augment_array.py

This script contains all critical code for augmenting the arrays

Written by: Alex K. Chew (05/05/2020)
"""
import os
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import cm
import math
import scipy as sp

## IMPORTING MODULES
import core.plotting_scripts as plotter

## DEFINING PERMUTATION LIST (23 unique lists, other than itself)
CUBE_PERMUTATION_LIST=[
        'x',
        'y',
        'xx',
        
        'xy',
        'yx',
        'yy',
        'xxx',
        
        'xxy',
        'xyx',
        'xyy',
        'yxx',
        
        'yyx',
        'yyy',
        'xxxy',
        'xxyx',
        
        'xxyy', # same as zz
        'xyxx',
        'xyyy',
        'yxxx',
        
        'yyyx',
        'xxxyx', # same as zzz
        'xyxxx', # same as z
        'xyyyx', 
        ]

#### FUNCTION TO PLOT ALL AUGMENTED ARRAYS
def plot_all_augmented_arrays(array_list,
                              max_cols = 6,
                              default_vox_rep=(4,3),
                              figsize = None,
                              want_title = True):
    '''
    This function plots all augmented arrays.
    INPUTS:
        array_list: [dict]
            dictionary containing the array
        default_vox_rep: [tuple]
            default voxel representation
        want_title:: [logical]
            True if you want title
    OUTPUTS:
        fig, ax:
            figure and axis for the plot
    '''
    
    ## GETTING NUM ROWS
    max_rows = math.ceil(len(array_list) / max_cols)
    
    ## SEEING IF YOU HAVE A PLANAR
    if len(array_list) == 4:
        print("Since length of array is 4, assuming planar")
        projection = None
        is_planar = True
    else:
        projection = '3d'
        is_planar = False
    
    ## DEFINING FIGURE SIZE
    if figsize is None:
        figsize=(default_vox_rep[0]*max_cols, default_vox_rep[1]*max_rows)
    
    for idx, keys in enumerate(array_list.keys()):
        print("Working on %s (%d of %d)"%(keys, idx+1, len(array_list)) )
        ## DEFINING RGB ARRAY
        if is_planar is True:
            rgb_array = array_list[keys][0]
        else:
            rgb_array = array_list[keys]
        
        
        print(rgb_array.shape)
        if idx == 0:
            ## CREATING FIGUURE
            fig = plt.figure(figsize = figsize)
            
        ## ADDING SUBPLOT
        ax = fig.add_subplot(max_rows, max_cols, idx+1, projection=projection)
        ## PLOTTING
        fig, ax = plotter.plot_voxel_split(rgb_array,
                                   alpha=0.15,
                                   want_renormalize = True,
                                   want_separate_axis = False,
                                   want_debug = False,
                                   want_round = True,
                                   voxel_cutoff = -1,
                                   fig = fig,
                                   ax = ax)
        ## TURNING OFF LABELS
        plotter.turn_ax_labels_off(ax)
        
        ## ADDING TITLE
        if want_title is True:
            ax.set_title(keys)
    
    ## TIGHT LAYOUT
    fig.tight_layout()
    
    return fig, ax

### FUNCTION TO CHECK IF ANY AUGMENTED ARRAY IS THE SAME
def check_augment_array_similarity(augment_array):
    '''
    The purpose of this function is to check if the augmented arrays 
    have any similarities.
    INPUTS:
        augment_array: [np.array, shape = (N, 20, 20, 20, 3)]
            Augmented array
    OUTPUTS:
        similarity_matrix: [np.array, shape=(N,N)]
            True/False based on whether the arrays are similar
    NOTE:
        The similarity array should be upper triangular. We are ignoring 
        self-terms (i, i) since they are obviously equal. 
        
        Ideally, the similarity matrix should sum to 0 -- which means that 
        no matrices are the same.
    '''
    ## CREATING EMPTY ARRAY
    similarity_matrix = np.full((len(augment_array), len(augment_array)), False)
    
    ## LOOPING THROUGH
    for i in range(len(augment_array)):
        ## LOOPING THROUGH J
        for j in range(i + 1, len(augment_array) ):
            ## GETTING SIMILARITY
            similarity = np.array_equal( augment_array[i], augment_array[j] )
            
            ## STORING
            similarity_matrix[i,j] = similarity
            
    return similarity_matrix

### FUNCTION TO CHECK EQUAL KEYS
def check_keys_similar(x_data,
                       x_augment,
                       array_list,
                       current_string = 'x'
                       ):
    '''
    The purpose of this function is to see if any keys in the augmented 
    array is similar to your proposed string.
    INPUTS:
        rotated_matrix: [np.array]
            rotated matrix
        x_augment: [np.array]
            x data but with augmented arrays
        array_list: [dict]
            dictionary with keys indicating for each array, e.g. 'x'
        current_string: [str]
            current string you are interested in
    OUTPUTS:
        idx_equal: [list]
            list of equivalent matrices
    Ideally, idx_equal has one index, indicating that the array is within 
    the augmented array. 
    '''
    ## CHECKING WHICH ONE IS EQUAL TO Z
    rotated_matrix = rotate_image_from_xyz_string(x_data, string = current_string)
    
    ## FINDING WHERE IS EQUAL
    idx_equal = [ idx for idx, each_array in enumerate(x_augment) if np.array_equal(each_array, rotated_matrix[0]) == True ]
    
    ## GETTING KEY
    key_list = list(array_list)
    ## FINDING KEYS
    equivalent_keys = [ key_list[each_idx] for each_idx in idx_equal ]
    
    print("Total equal matrices: %d"%(len(idx_equal) ) )
    print("Current string: %s"%(current_string) )
    print("Keys that are the same: %s"%(', '.join(equivalent_keys) ) )
    return idx_equal

### FUNCTION TO ROTATE IMAGE
def rotate_image_90_180_270(data, axis_type = (1,2)):
    '''
    This function rotates an image in 90 degree increments for a given axis.
    INPUTS:
        data: [np.array, shape = (N, 20, 20, 20, 3)]
            data you are trying to rotate
        axis_type: [tuple, 2]
            axis you are trying to rotate along
    OUTPUTS:
        rotate_90, rotate_180, rotate_270:
            numpy arrays with 90, 180, and 270 degree rotations for a specified axis.
    '''
    rotate_90 = sp.ndimage.interpolation.rotate(data, 90, axis_type)
    rotate_180 = sp.ndimage.interpolation.rotate(rotate_90, 90, axis_type)
    rotate_270 = sp.ndimage.interpolation.rotate(rotate_180, 90, axis_type)
    
    return rotate_90, rotate_180, rotate_270
    
### FUNCTION TO ROTATE IMAGE BASED ON STRING
def rotate_image_from_xyz_string(matrix, 
                                 string = "x",
                                 rotation_code="numpy"):
    '''
    The purpose of this function is to rotate an image based on a string.
    All rotations are done with counter clockwise 90 degrees. For example, 
    "x" would mean to rotate around the x-axis in 90 degrees. If it is
    "xy", then we rotate 90 degrees around the x-axis first, then another 
    90 degrees around the y-axis. This function is useful to developing 
    24 unique cube rotations.
    INPUTS:
        matrix: [np.array, shape=(N,20,20,20,3)]
            data you are trying to rotate
        string: [str]
            array of strings, e.g. 'x', 'y', 'z'
        rotation_code: [str]
            rotation code used, either "numpy" or "scipy"
            Note that scipy has some errors with interpolation. As a result, 
            rotations with scipy may not be reliable. These result in arrays 
            with small number differences. 
    OUTPUTS:
        rotated_matrix: [np.array, shape=(N,20,20,20)]
            Rotated matrix
    '''
    ## GETTING LIST OF ARRAY
    array_string = list(string)
    
    ## GETTING AXIS FOR EACH
    axis_list = []
    for each_array in array_string:
        if each_array == 'x':
            axis_type = (2,3)
        elif each_array == 'y':
            axis_type = (1,3)
        elif each_array == 'z':
            axis_type = (1,2)
        else:
            print("Error in rotating image! String (%s) is not defined!"%(string))
            print("Strings that are defined: x, y, z")
        ## STORING AXIS TYPE
        axis_list.append(axis_type)
        
    ## ROTATING EACH AXIS BY 90
    rotated_matrix = matrix.copy() # Copy rotated matrix
    for current_axis in axis_list:
        if rotation_code == "scipy":
            rotated_matrix = sp.ndimage.interpolation.rotate(rotated_matrix, 90, current_axis)
        elif rotation_code == "numpy":
            rotated_matrix = np.rot90( rotated_matrix, k=1, axes = current_axis)

    return rotated_matrix
    
def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

### FUNCTION TO AUGMENT DATA
def augment_data_90_increments(x_train, 
                               y_train, 
                               data_shape = (32, 32, 32, 3),
                               return_dict = False):
    '''
    The purpose of this function is to augment the training data. It does the following:
        -Rotate training 90 degrees in x- direction
        -Rotate training 90 degrees in y- direction
        -Rotate training 90 degrees in the z direction
        -Contatenate all possible training data
        -Add training Y-values 4x
        
        The updated version rotates in 24 rotations around a cube. In addition,
        updated version uses 90 degree rotations from numpy
        
    NOTE:
        This augmentation correctly recreates the rotations, whereas the 
        'augment_data' function loses data. This behavior is faintly 
        mentioned in the lossy data link:
            https://github.com/scipy/scipy/issues/1903
    INPUTS:
        x_train: [np.array]
            x training data set
        y_train: [np.array]
            y training set
        return_dict: [logical]
            True if you want the dictionary of each augmented array
    OUTPUTS:
        x_train: [np.array]
            updated x training data set
        y_train: [np.array]
            updated y training set
        num_tile: [int]
            number of tiles required due to augmentation
    REFERENCES:
        24 unique cube permutations:
            https://www.euclideanspace.com/maths/discrete/groups/categorise/finite/cube/index.htm
    '''
    ## LOOPING AND GENERATING ARRAYS
    array_list = {}
    # 3D ARRAY
    if len(data_shape) == 4:
        print("Since 3D array, we are rotating 24 times!")
        string_codes = CUBE_PERMUTATION_LIST
    # 2D ARRAY
    elif len(data_shape) == 3:
        ## DEFINING PLANAR STRING CODE
        string_codes = ['z', 'zz', 'zzz']
        print("Since length of data set is 3, we have a planar image.")
        print("String codes for augmentation: %s"%(', '.join(string_codes) ) )
        
    ## LOOPING THROUGH ALL POSSIBLE PERMUTATIONS
    for each_string in string_codes:
        ## GETTING PERMUTATION
        rotated_matrix = rotate_image_from_xyz_string(x_train, 
                                                      string = each_string,
                                                      rotation_code = "numpy")
        ## STORING
        array_list[each_string] = rotated_matrix
        
    ## CONVERTING TO ARRAY
    array_augmented = np.concatenate([array_list[each_key] for each_key in array_list])
    
    ## COMBINING AUGMENTED WITH ORIGINAL ARRAY
    x_augmented = np.concatenate((x_train, 
                                  array_augmented
                                  ), axis=0)
    ## DEFINING NUMBER OF TILE FOR Y TRAIN
    num_tile = int( len(x_augmented) / len(x_train) )

    ## AUGMENTING Y VALUES    
    y_train = np.tile( y_train, num_tile )
    
    if return_dict is False:
        return x_augmented, y_train, num_tile
    else:
        ## STORING THE IDENTITY
        identity_dict = { 'i': x_train }
        output_dict = merge_two_dicts(identity_dict, array_list)        
        return x_augmented, y_train, num_tile, output_dict
    
    ''' OLD CODE USED TO ROTATED 9 TIMES INSTEAD OF 24
#            ## AUGMENT ALONG XY PLANE (Z AXIS)
#            x_train_xy_1, x_train_xy_2, x_train_xy_3 = rotate_image_90_180_270(data = x_train, 
#                                                                               axis_type = (1,2))
#            
#            ## AUGMENT ALONG THE XZ PLANE (Y AXIS)
#            x_train_xz_1, x_train_xz_2, x_train_xz_3 = rotate_image_90_180_270(data = x_train, 
#                                                                               axis_type = (1,3))
#            
#            ## AUGMENT ALONG THE YZ PLANE (X AXIS)
#            x_train_yz_1, x_train_yz_2, x_train_yz_3 = rotate_image_90_180_270(data = x_train, 
#                                                                               axis_type = (2,3))
#            
#            
#            ## AUGMENTED ALONG EACH AXIS
#            ## DEFINING LIST
#            array_list = [[x_train_xy_1, x_train_xy_2, x_train_xy_3,],
#                          [x_train_xz_1, x_train_xz_2, x_train_xz_3,],
#                          [x_train_yz_1, x_train_yz_2, x_train_yz_3,] ]
#            
#            
#            ## DEFINING AXIS LIST
#            axis_list = [ [ (1,3), (2,3) ],
#                          [ (1,2), (2,3) ],
#                          [ (1,2), (1,3) ],
#                           ]
#            
#            ## CREATING A NEW LIST
#            augment_along_all_axis = []
#            
#            ## LOOPING THROUGH EACH ARRAY
#            for idx_array, current_list_of_arrays in enumerate(array_list):
#                
#                ## LOOPING PER ARRAY
#                for each_array in current_list_of_arrays:
#                
#                    ## LOOPING THROUGH EACH AXIS
#                    for idx_axis, current_axis in axis_list[idx_array]:
#                        
#                        ## ROTATING
#                        rotate_90, rotate_180, rotate_270 = rotate_image_90_180_270(data = x_train, 
#                                                                                    axis_type = (1,2))
#                        
#                        ## STORING
#                        augment_along_all_axis.extend([rotate_90, rotate_180, rotate_270])
#            augment_along_all_axis = np.concatenate(augment_along_all_axis, axis = 0)
        # print(augment_along_all_axis.shape)
        ## TRAINING SET CONCATENATION
#            x_train = np.concatenate((x_train, 
#                                      x_train_xy_1, x_train_xy_2, x_train_xy_3,
#                                      x_train_xz_1, x_train_xz_2, x_train_xz_3,
#                                      x_train_yz_1, x_train_yz_2, x_train_yz_3,
#                                      augment_along_all_axis,
#                                      ), axis=0)
        
        '''
#%%

## MAIN FUNCTION
if __name__ == "__main__":
    
    
    ## loading from publishable images
    from publishable_images import path_dict, load_instances_based_on_pickle
    
    ## FINDING SPECIFIC INSTANCE
    ## LOADING SPECIFIC 
    database_name="20_20_20_20ns_oxy_3chan"
    # "32_32_32_20ns_oxy_3chan_firstwith10"
    # 
    
    pickle_location= os.path.join(path_dict['combined_database_path'])
    pickle_name= database_name + r"-split_avg_nonorm-10-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-DIO_GVL_THF-10_25_50_75"
    
    # pickle_name = "32_32_32_20ns_oxy_3chan_firstwith10-split_avg_nonorm_planar-10-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-DIO_GVL_THF-10_25_50_75"
    ## LOADING INSTANCES
    instances = load_instances_based_on_pickle(pickle_name = pickle_name ,
                                               path_combined_database = path_dict['combined_database_path'])
    
    #%% AUGMENTING SPECIFIC PARTITION
    
    
    ## SPECIFIC INDEX
    specific_instance="XYL_403.15_DIO_10"
    index_instance = instances.instance_names.index( specific_instance )
    
    ## SPECIFIC PARTITION
    specific_partition=0
    
    x_data = np.array(instances.x_data)[0:2] # [specific_partition]
    y_data = np.array(instances.y_label)[0:2]
    
    
    ## GETTING Y ARRAY
    
    ## COMBINING X DATA
    # x_data = np.concatenate(x_data)
    
    from train_deep_cnn import split_train_test_set
    
    x_train, x_test, y_train, y_test = split_train_test_set(sampling_dict={'name': 'strlearn',
                                                                           'split_percentage': 0.8},
                                                            instances = instances,)
    
    ## defining input data shape
    input_data_shape = instances.x_data[0][0].shape
    
    ## AUGMENTING BY 90 DEGREES
    x_augment, y_augment, num_tile, array_list = augment_data_90_increments(x_train = x_train, 
                                                                y_train = y_train,
                                                                data_shape = input_data_shape,
                                                                return_dict = True)
    
    
    #%% SEEING IF ANY ROTATIONS ARE SIMILAR
    
    ## CHECKING
    idx_equal = check_keys_similar(x_data = x_data,
                                   x_augment = x_augment,
                                   array_list = array_list,
                                   current_string =  "z"
                                   )
    
    
    #%% CHECKING SIMILARITY OF AUGMENTATION
    
    ## CHECKING SIMILARITY MATRIX
    similarity_matrix = check_augment_array_similarity(augment_array = x_augment)
    print(np.sum(similarity_matrix))
    
    #%%## PLOTTING ALL AUGMENTED ARRAYS
    fig, ax = plot_all_augmented_arrays(array_list = array_list,
                                        max_cols = 6,
                                        default_vox_rep=(4,3),)
            
    
    
    
    