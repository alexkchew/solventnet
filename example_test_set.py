# -*- coding: utf-8 -*-
"""
plotting_spatial_correlations.py
The purpose of this code is to load the SolventNet model and the instances, then 
generate spatial correlation maps that could give insight into what the 3D CNN 
is doing. This code was designed for Shengli (Bruce) Jiang to make maps. 

Created on: 02/10/2020

"""
## IMPORTING OS COMMANDS
import os

## TRAINING TOOLS
from train_deep_cnn import split_train_test_set

## ANALYSIS TOOLS
from analyze_deep_cnn import find_avg_std_predictions, create_dataframe

## PICKLE FUNCTIONS
from extraction_scripts import load_pickle_general

## LOADING THE MODELS
from keras.models import load_model

## LOADING NOMENCLATURE
from core.nomenclature import read_combined_name, extract_sampling_inputs, extract_instance_names

## PLOTTING PARITY PLOT
from read_extract_deep_cnn import plot_parity_publication_single_solvent_system

import core.plotting_scripts as plotter

#%%

## MAIN FUNCTION
if __name__ == "__main__":
    ## DEFINING PATH TO SIMULATION FOLDER
    sim_folder=r"R:\scratch\3d_cnn_project\simulations"
    
    ## DEFINING SPECIFIC SIM
    specific_sim = "20200327-Alex_S_testsets"
    # r"20200210-SolventNet_For_Bruce"
    ## DEFINING INSTANCES FILE
    instances_file = "3D_CNN_TEST_SETS.pickle" 
    
    ## DEFINING PATHS
    path_to_sim = os.path.join(sim_folder,
                               specific_sim)
    
    ## DEFINING PATH TO INSTANCES
    path_instances = os.path.join(path_to_sim,
                                  instances_file)
    
    #%% LOADING ALL INSTANCES
    instances = load_pickle_general(path_instances)
    '''
    instances outputs everything of shape 3:
        Index 0: voxel representations in 76 x 10 x (20 x 20 x 20 x 3)
            where each reactant/water has 10 partitions (i.e. 10 voxel representations)
            The last 2 voxel representations are used for testing
        Index 1: Experimental sigma values in with length of 76, e.g.
            [1.15,
             0.84,
             0.21,
             0.05,
             1.6,
             0.72,
             ...
             ]
            
        Index 2: Labels for each, e.g.
            ['FRU_393.15_ACE_75',
             'FRU_393.15_ACE_56',
             'FRU_393.15_ACE_35',
             'FRU_393.15_ACE_12',
             ...
             ]
            Where 'FRU_393.15_ACE_75' means cellobiose simulated at 393.15 K, with 75 wt% water / 25 wt% acetone
            Other cosolvents:
                ACN: acetonitrile
                ACE: acetone
                dmso: dimethylsulfoxide
    '''
    
    #%%
    
    ## SEPARATING INFORMATION
    xlabel, ylabel, instancenames = instances
    
    
    #%%
    
    ## PLOTTING VOXEL REPRESENTATIONS
    fig, ax = plotter.plot_voxel_split(xlabel[0][0],
                                 want_renormalize=True,
                                 )
    '''
    Plots voxel representation for FRU in 393.15 K, Acetone in 75 wt% water / 25 wt% cosolvent
    '''
    