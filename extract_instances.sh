#!/bin/bash
# extract_instances.sh
# The purpose of this script is to generate instances if necessary. We can then 
# use the instances to copy over files and run subsequent deep CNN. Note that 
# the goal of this script is simply to generate initial instance that is then copied 
# over to the current directory.
# 
# Written by: Alex K. Chew (02/21/2020) 

## DEFINING PYTHON SCRIPT
python_naming_script="_PYTHONNAME_"
python_instances_script="_PYTHONINSTANCES_"

## DEFINING REPRESENTATION TYPE
representation_types="_REPTYPE_" # split_avg_nonorm split_average
representation_inputs="_REPINPUT_" # Number of splits

## DEFINING SAMPLING TYPE
sampling_type="_SAMPLETYPE_"
sampling_inputs="_SAMPLEINPUTS_"

## DEFINING MASS FRACTION
mass_frac="_MASSFRAC_" # 75
cosolvents="_COSOLVENT_" # DIO,GVL,THF
solutes="_SOLUTES_"

## RUNNING PYTHON SCRIPT TO GET THE NAME





