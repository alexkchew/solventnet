# -*- coding: utf-8 -*-
"""
generate_cross_validation_fold_indices.py
The purpose of this python code is to generate cross validation fold indices given 
the instances array. The idea is to quickly generate the indices so you could 
perform cross validation analysis. 

Written by: Alex K. Chew (02/21/2020)

"""
import os
import numpy as np
## CHECKING TOOLS
from core.check_tools import check_testing
from core.pickle_tools import pickle_results, load_pickle_results
## IMPORTING COMBINING ARRAYS
from combining_arrays import combine_instances

## K FOLD MODEL SELECTION
from sklearn.model_selection import KFold


### FUNCTION TO GET INDICES FOR K-FOLD LEARNING
def get_indices_for_k_fold_cross_validation(instances = None, 
                                            x = None,
                                            y = None,
                                            names = None,
                                            n_splits = 5, 
                                            verbose = False ):
    '''
    The purpose of this function is to get the k-fold cross validation index 
    for cross validation training. 
    INPUTS:
        instances: [obj]
            instances object
        x: [np.array]
            x data to train on
        y: [np.array]
            y data to train on
        names: [list]
            list ofnames that you are training on
        n_splits: [int, default = 5]
            number of splits for your x, y data
        verbose: [logical, default = False]
            print out details of splitting
    OUTPUTS:
        indices_dict: [dict]
            dictionary containing indices that you need to distinguish 
            training and validation/test set. 
        ## TO GET THE TRAINING AND VALIDATION INDICES
        x_train, x_val = x[train_index], x[test_index]
        y_train, y_val = y[train_index], y[test_index]
            
    '''
    ## IF INSTANCES ARE NOT NONE, RELOAD:
    if instances is not None:
    
        ## DEFINING X AND Y
        x = np.array(instances.x_data)
        y = np.array(instances.y_label)
        
        ## DEFINING NAMES
        names = np.array(instances.instance_names)
    
    ## CROSS VALIDATION
    skf = KFold(n_splits=n_splits, random_state=0, shuffle=True)
    skf.get_n_splits(x, y)
    
    ## DEFINING INDICES DICT
    indices_dict = []
    ## SPLITTING X AND Y, GETTING THE INDEXES
    for train_index, test_index in skf.split(x, y):
        np.random.seed(0)
        np.random.shuffle(train_index)
        np.random.seed(0)
        np.random.shuffle(test_index)
        ## DEFINING INSTANCE TRAINING AND TEST
        names_train_test = {
                'train_names': names[train_index],
                'test_names': names[test_index],
                'train_index' : train_index,
                'test_index': test_index
                }
        indices_dict.append(names_train_test)
        if verbose is True:
            print("Splitting training and testing index")
            print("TRAIN:", train_index, "TEST:", test_index)
            print("TRAIN NAME:", names_train_test['train_names'])
            print("TEST NAME:", names_train_test['test_names'])
            
    return indices_dict

### MAIN FUNCTION TO GENERATE INDICES
def main_generate_cross_validation_indices(output_path,
                                           pickle_name,
                                           num_cross_validation_folds,
                                           indices_pickle_name = "indices.pickle",
                                           indices_summary_name = "indices.summary",
                                           ):
    '''
    The purpose of this script is to generate cross validation fold indices 
    required to generate cross validation indices
    INPUTS:
        output_path: [str]
            output path
        pickle_name: [str]
            pickle name
        num_cross_validation_folds: [int]
            number of cross validation folds. If 0, no cross validation indices 
            will be generated.
        indices_pickle_name: [str]
            name of output pickle
    OUTPUTS:
        void -- this function outputs a pickle
    '''

    ## LOADING THE DATA
    instances = combine_instances(
                     output_path = output_path,
                     pickle_name = pickle_name,
                     )
    
    ## CHECKING IF THE NMBER OF CROSS VALIDATION FOLDS IS NOT ZERO
    if num_cross_validation_folds != 0:
        ## GETTING INDICES
        indices_dict = get_indices_for_k_fold_cross_validation( instances, 
                                                                n_splits = num_cross_validation_folds,
                                                                verbose = False)
    else:
        ## PRINTING NO INDICES
        indices_dict = []
        print("Since num_cross_validation_folds is zero, setting indices dict to empty list")
        
    ## DEFINING PATH TO INDICES
    path_indices = os.path.join(output_path,indices_pickle_name)
    
    ## STORING
    pickle_results(results = indices_dict,
                   pickle_path = path_indices)
        
    ## WRITING SUMMARY
    path_summary = os.path.join(output_path,indices_summary_name)
        
    ## PRINTING
    with open(path_summary, 'w') as f:
        ## WRITING
        f.write("Length of cross validation folds: %d"%(num_cross_validation_folds))
        
    
    ''' Testing reloading

    ## RESTORING PICKLE
    print("Reloaded indices")
    indices_reloaded = load_pickle_results(file_path = path_indices)
    print(indices_reloaded)
    '''
    return


#%% MAIN FUNCTION
if __name__ == "__main__":
    
    ## SEE IF TESTING IS ON
    testing = check_testing()
    
    if testing == True:
        ## DEFINING PATH TO INSTANCE 
        output_path = r"R:\scratch\3d_cnn_project\simulations\20200221-5fold_train_20_20_20_20ns_oxy_3chan\20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-strlearn-0.80-solvent_net-500-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-10_25_50_75-DIO_GVL_THF"
        pickle_name = r"20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-DIO_GVL_THF-10_25_50_75"
        ## DEFINING NUMBER OF CROSS VALIDATIONS
        num_cross_validation_folds = 5
        ## DEFINING INDICES NAMES
        indices_pickle_name="indices.pickle"
    else:
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## DEFINING OUTPUT PICKLE
        parser.add_option('--output_path', dest = 'output_path', help = 'Full path output the pickle file', default = '')

        ## DEFINING OUTPUT PICKLE
        parser.add_option('--instances_pickle', dest = 'pickle_name', help = 'Name of the instances pickle', default = '')        
        
        ## DEFINING OUTPUT PICKLE
        parser.add_option('--indices_prefix', dest = 'indices_prefix', help = 'Name of the output indices prefix', default = '')        
        
        ## CROSS VALIDATION FOLDS
        parser.add_option("--num_cross_folds", dest="num_cross_validation_folds", help = "Number of cross validation folds", type=int, default = 1 ) 
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## STORING OUTPUT PATH
        output_path = options.output_path
        ## DEFINING PICKLE
        pickle_name = options.pickle_name
        indices_prefix = options.indices_prefix
        ## CROSS VALIDATION
        num_cross_validation_folds = options.num_cross_validation_folds
        
    ## RUNNING GENERATE CROSS VALIDATION INDICES
    main_generate_cross_validation_indices(output_path = output_path,
                                           pickle_name = pickle_name,
                                           num_cross_validation_folds = num_cross_validation_folds,
                                           indices_pickle_name = indices_prefix + ".pickle",
                                           indices_summary_name = indices_prefix + ".summary",
                                           )


        
        