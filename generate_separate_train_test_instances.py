# -*- coding: utf-8 -*-
"""
generate_separate_train_test_instances.py

The purpose of this script is to generate training and testing instances 
that are separte. This is useful if you want to check if training on a subset 
and testing on the remaining set performs well. This was suggested by Reid 
to try to alleviate some criticism on separate training / validation / test set 
partitions. 

Written by: Alex K. Chew (05/06/2020)

"""
import os
import copy

## IMPORTING FUNCTIONS
from generate_cross_validation_fold_indices import get_indices_for_k_fold_cross_validation

### FUNCTION TO CREATE NEW INSTANCES BASED ON INDEX
def create_new_instance_based_on_index(instances,
                                       index):
    '''
    The purpose of this function is to create a new instance object based on 
    the index supplied. The idea is to overwrite the x_data, y_label, and 
    instance_names so you will have a new instance object. 
    As a result, you could create potential subsets of instances, and use them 
    to train specific data sets. In addition, this could be used to generate 
    test set instances.
    INPUTS:
        instances: [obj]
            instances object from combining_arrays
        index: [array]
            array of indexes to extract
    OUTPUTS:
        new_instances: [obj]
            updated instance object
    '''
    ## COPYING INSTANCES
    new_instances = copy.deepcopy(instances)
    
    ## OVERWRITING NEW OBJECTS X DATA, ETC.
    new_instances.x_data = [ instances.x_data[each_index] for each_index in index]
    new_instances.y_label = [ instances.y_label[each_index] for each_index in index]
    new_instances.instance_names = [ instances.instance_names[each_index] for each_index in index]
    
    return new_instances



#%%
## MAIN FUNCTION
if __name__ == "__main__":
    
    ## loading from publishable images
    from publishable_images import path_dict, load_instances_based_on_pickle

    ## FINDING SPECIFIC INSTANCE
    ## LOADING SPECIFIC 
    database_name="20_20_20_20ns_oxy_3chan"
    
    
    pickle_location= os.path.join(path_dict['combined_database_path'])
    pickle_name= database_name + r"-split_avg_nonorm-10-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-DIO_GVL_THF-10_25_50_75"
    
    ## DEFINING PATH TO DATABASE
    path_combined_database =  path_dict['combined_database_path']
    
    # pickle_name = "32_32_32_20ns_oxy_3chan_firstwith10-split_avg_nonorm_planar-10-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-DIO_GVL_THF-10_25_50_75"
    ## LOADING INSTANCES
    instances = load_instances_based_on_pickle(pickle_name = pickle_name ,
                                               path_combined_database = path_combined_database)
    
    #%%
    

    
    ## DEFINING NUMBER OF SPLITS
    num_splits=5
    
    ## GETTING INDICES
    indices = get_indices_for_k_fold_cross_validation(instances = instances,
                                                      n_splits = num_splits
                                                      )
    
    #%%
    
    ## PICKLE NAMES
    split_pickle_name = instances.pickle_name.split('-')
    
    ## LOOPING THROUGH EACH INDEX AND GENERATING NEW OBJECT
    for idx, each_index_dict in enumerate(indices):
        ## FINDING ALL INDEXES
        train_index = each_index_dict['train_index']
        test_index = each_index_dict['test_index']
        
        ## DEFINING NAME
        input_name="%d_%d"%(idx, num_splits)
        
        ## OVERWRITING NEW OBJECTS X DATA, ETC.
        train_instances = create_new_instance_based_on_index(instances = instances,
                                                             index = train_index)
        ## OVERWRITING NEW OBJECTS X DATA, ETC.
        test_instances = create_new_instance_based_on_index(instances = instances,
                                                             index = test_index)
        
        ## GETTING NEW NAME
        train_pickle_name ='-'.join([ '_'.join( [split_pickle_name[0], 'train', input_name ] )] +  split_pickle_name[1:] )
        test_pickle_name ='-'.join([ '_'.join( [split_pickle_name[0], 'test', input_name ] ) ] +  split_pickle_name[1:]  )
        
        ## STORING PICKLE FILE
        path_pickle = path_combined_database
        path_train_pickle = os.path.join(path_pickle,
                                         train_pickle_name)
        path_test_pickle = os.path.join(path_pickle,
                                         test_pickle_name)
        
        ## STORING
        train_instances.store_pickle(path = path_train_pickle)
        test_instances.store_pickle(path = path_test_pickle)
