#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
generate_test_set_combined_arrays.py

This script is generated to create test set instance for Alex S. The idea is to 
extract the individual voxel frames and combine them to generate a voxel representation. 
We will then test the performance using SolventNet to see its utility, as compared 
to previous work.

Written by: Alex K. Chew (03/27/2020)

"""
import os
from combining_arrays import combine_instances
from core.global_vars import PATH_MAIN_PROJECT as MAIN_PATH




#%%

## MAIN FUNCTION
if __name__ == "__main__":
    
    ## DEFINING NONE LIST
    solvent_list = None
    mass_frac_data = None
    
    ## DEFINING TYPE OF REPRESENTATION
    representation_type = 'split_avg_nonorm' 
    
    representation_inputs = {
            'num_splits': 10,
            }
    
    ## DEFINING PICKLE NAME
    pickle_name="3D_CNN_TEST_SETS.pickle"
    
    ## DEFINING VERBOSITY
    verbose = True
    
    ## TURNING OFF PICKLEING
    enable_pickle = True # false
    skip_loading = False
    load_based_on_csv = True
    
    ## DEFINING PATH OF COMBINING DATA
    combined_database_path = os.path.join(MAIN_PATH,"combined_data_set")
    ## DEFINING DATABASE PATH
    database_path= os.path.join(MAIN_PATH,"database", "20_20_20_20ns_oxy_3chan_TEST_SET")
    ## DEFINING CLASS PATH
    class_file_path =os.path.join(MAIN_PATH,"database",
                                  "Experimental_Data",
                                  "solvent_effects_regression_data_testset.csv")
    
    ## DEFINING NOUTPUT PATH
    output_path = os.path.join(MAIN_PATH,"database")
    
    ## DEFINING DATATYPE
    data_type = "20_20_20_20ns_oxy_3chan_TEST_SET"
    
    ## COMBINING INSTANCES
    instances = combine_instances(
                     solute_list = None,
                     representation_type = representation_type,
                     representation_inputs = representation_inputs,
                     solvent_list = solvent_list, 
                     mass_frac_data = mass_frac_data, 
                     verbose = verbose,
                     database_path = database_path,
                     class_file_path = class_file_path,
                     combined_database_path = combined_database_path,
                     data_type = data_type,
                     enable_pickle = enable_pickle, # True if you want pickle on
                     output_path = output_path,
                     pickle_name = pickle_name,
                     skip_loading = skip_loading,
                     load_based_on_csv = load_based_on_csv,
                     )