#!/bin/bash
# loop_grid_interpolation.sh
# This code uses the looping algorithm for the grid inteprolation

# Written by: Alex K. Chew (03/07/2019)

## USAGE EXAMPLE: ./bayes tic-tac-toe_sub_train.json tic-tac-toe_sub_test.json n > outfile.txt

# alias python3=/usr/bin/python3.4

## DEFINING PYTHON SCRIPT
python_script="loop_grid_interpolation.py"

## DEFINING INPUT VARIABLES
# TRAJ LOCATION

#database_name="20_20_20_50ns"
#database_name="20_20_20_10ns_allatomwithsoluteoxygen"
#database_name="20_20_20_10ns_updated"
#database_name="20_20_20_50ns_updated"
#database_name="20_20_20_100ns_updated"
#database_name="20_20_20_40ns_first"
#database_name="20_20_20_32ns_first_new_MeCN"
## ---- UPDATED DATBASES ---- #
#database_name="20_20_20_32ns_first"

# GRO XTC FILE
gro_file="mixed_solv_prod.gro"
# xtc_file="mixed_solv_prod_first_32_ns_centered.xtc"
xtc_file="mixed_solv_prod_first_20_ns_centered_with_10ns.xtc"
# ---
# xtc_file="mixed_solv_prod_first_32_ns_centered_with_10ns.xtc"
# xtc_file="mixed_solv_prod_first_100_ns_centered_with_10ns.xtc"
# xtc_file="mixed_solv_prod_first_100_ns_centered.xtc"
# xtc_file="mixed_solv_prod_last_100_ns_centered.xtc"
# xtc_file="mixed_solv_prod_first_40_ns_centered.xtc"
# mixed_solv_prod_last_10_ns_centered.xtc
# xtc_file="mixed_solv_prod_last_190_ns_centered.xtc"
# xtc_file="mixed_solv_prod_last_50_ns_centered.xtc"

## DEFINING DATABASE PREFIX
database_prefix="20_20_20_20ns_oxy_3chan"
# 32_32_32_20ns
# "20_20_20_20ns"
# "32_32_32_20ns"
# "16_16_16_20ns"
# "20_20_20_20ns"
# "20_20_20_32ns"

## DEFINING DETAILS OF SIMULATIONS
input_traj_loc="/home/akchew/scratch/SideProjectHuber/Analysis/170814-7Molecules_200ns_Full"
mass_frac="10,25,50,75" # 75
solutes="CEL,ETBE,FRU,LGA,PDO,XYL,tBuOH"
cosolvents="DIO,GVL,THF" #  DIO,GVL,THF ,dmso ,GVL,THF ,dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso ,GVL,THF
#
database_name="${database_prefix}_firstwith10" # _firstwith10"
# "20_20_20_32ns_firstwith10"
## "20_20_20_110_origin"
## "32_32_32_32ns_first"
temperatures="403.15,343.15,373.15,403.15,433.15,403.15,363.15"

##### FOR NATURE CATALYSIS DMSO WORK
input_traj_loc="/home/akchew/scratch/SideProjectHuber/Analysis/190305-TBA_FRU_MIXED_SOLV_DMSO"
mass_frac="10,25,50,75" # 75
solutes="FRU,PDO,tBuOH"
cosolvents="dmso" #  DIO,GVL,THF ,dmso ,GVL,THF ,dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso ,GVL,THFq
temperatures="373.15,433.15,363.15"
database_name="${database_prefix}_firstwith10_dmso"
##
###### FOR FRUCTOSE ALI'S WORK
input_traj_loc="/home/akchew/scratch/SideProjectHuber/Analysis/190612-FRU_HMF_Run"
mass_frac="12,35,56,75" # 75
cosolvents="ACE" #  DIO,GVL,THF ,dmso ,GVL,THF ,dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso ,GVL,THF
solutes="FRU,GLU"
temperatures="393.15,393.15"
database_name="${database_prefix}_firstwith10_ACE"
##
###### FOR NAT CATALYSIS MECN WORK
input_traj_loc="/home/akchew/scratch/SideProjectHuber/Analysis/190614-Run_MeCN"
mass_frac="10,25,50,75" # 75
cosolvents="ACN" #  DIO,GVL,THF ,dmso ,GVL,THF ,dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso ,GVL,THF
solutes="FRU,PDO,tBuOH" # "FRU,GLU"
temperatures="373.15,393.15,363.15"
database_name="${database_prefix}_firstwith10_ACN"

# -----------------------
# DESIRED DATABASE LOCATION
# output_database_loc="/home/akchew/scratch/3d_cnn_project/database/20_20_20_190ns"
output_database_loc="/home/akchew/scratch/3d_cnn_project/database/${database_name}"

## MAKING DIRECTORY
mkdir -p "${output_database_loc}"

# BOX INFORMATION
box_size=4.0
box_inc=0.2 # 0.13333333333 # 0.2 # 0.125
#   0.4 -- 10 x 10 x 10
#   0.25 -- 16 x 16 x 16
#   0.2 -- 20 x 20 x 20
#   0.13333333333 -- 30 x 30 x 30
#   0.125 -- 32 x 32 x 32
box_map_type="3channel_oxy"
# "allatomwithsoluteoxygen" # <-- make sure to change to allatom if you don't want oxygen
# allatom
# allatomwithsoluteoxygen

## NORMALIZATION
normalization="maxima" # maxima rdf

## RUNNING PYTHON SCRIPT
# python3 
# /usr/bin/python3.4
 /usr/bin/python3.4 "${python_script}" -i "${input_traj_loc}" \
                                       -o "${output_database_loc}" \
                                       -g "${gro_file}" \
                                       --xtcfile "${xtc_file}" \
                                       -c "${box_inc}" \
                                       -b "${box_size}" \
                                       -t "${box_map_type}" \
                                       -n "${normalization}" \
                                       -m "${mass_frac}" \
                                       --solvent "${cosolvents}" \
                                       -s "${solutes}" \
                                       --temp "${temperatures}" \
                                       
