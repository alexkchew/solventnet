# -*- coding: utf-8 -*-
"""
output_instance_name.py
The purpose of this script is to outpu the instance name. This is useful for 
when you want to copy and instance into a directory, then perform computations 
with the instance available.

Written by: Alex K. Chew (02/21/2020)

"""

## CHECKING TOOLS
from core.check_tools import check_testing

## IMPORTING GLOBAL VARIABLES
from core.global_vars import SOLUTE_TO_TEMP_DICT

## IMPORTING FUNCTIONS
from core.nomenclature import get_combined_name, read_combined_name, extract_representation_inputs

## IMPORTING INSTANCES FUNCTION
from combining_arrays import add_parser_options_instance_details

### MAIN FUNCTION
def main_output_instance_name(representation_type,
                              representation_inputs,
                              solute_list,
                              mass_frac_data,
                              data_type,
                              path_output = None,
                              verbose = True):
    '''
    The purpose of this function is to output the instance names. 
    INPUTS:
        representation_type: [str]
            representation type
        representation_inputs: [dict]
            dictionary of representation inputs
        solute_list: [list]
            list of the solutes
        mass_frac_data: [list]
            list of the mass fraction of interest
        data_type: [str]
            data type you are using
        path_output: [str]
            path to the output file which you will store your pickle name
        verbose: [logical]
            True if you want to print out details
    '''
    
    ## FINDING NAME OF FILE
    pickle_name = get_combined_name(
                                     representation_type = representation_type,
                                     representation_inputs = representation_inputs,
                                     solute_list = solute_list,
                                     solvent_list = solvent_list,
                                     mass_frac_data = mass_frac_data,
                                     data_type = data_type
                                     )
    ## PRINTING TO OUTPUT
    if path_output is not None:
        ## PRINTING PICKLE NAME
        with open(path_output, 'w') as f:
            f.write(pickle_name)
        if verbose is True:
            print("Printing pickle name to: %s"%(path_output))
    return pickle_name

#%% MAIN FUNCTION
if __name__ == "__main__":
    
    ## SEE IF TESTING IS ON
    testing = check_testing()
    
    if testing == True:
    
        ## DEFINING SOLVENT LIST
        solvent_list = [ 'DIO', 'GVL', 'THF' ]
        ## DEFINING MASS FRACTION DATA
        mass_frac_data = ['10', '25', '50', '75']
        ## DEFINING SOLUTE LIST
        solute_list = list(SOLUTE_TO_TEMP_DICT)
        ## DEFINING TYPE OF REPRESENTATION
        representation_type = 'split_avg_nonorm' 
        
        representation_inputs = {
                'num_splits': 8,
                }
    
        ## DEFINING DATA TYPE
        data_type="20_20_20_40ns_first"
        
        ## DEFINING PATH TO OUTPUT
        path_output = r"R:\scratch\3d_cnn_project\simulations\output.txt"
        
    else:
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## ADDING PARSER
        parser = add_parser_options_instance_details(parser = parser)
        
        ## DEFINING DATA SET TYPE
        parser.add_option('--path_output', dest = 'path_output', help = 'path to output', type="string", default = "./output.txt")
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
    
        ### DEFINING ARUGMENT
        solute_list = options.solute_list
        solvent_list = options.solvent_list
        mass_frac_data = options.mass_frac_data
        ## REPRESENTATION
        representation_type = options.representation_type
        representation_inputs = options.representation_inputs
        ## DATA  SET TYPE
        data_type = options.data_type
        ## GETTTING PATH 
        path_output = options.path_output
        ## UPDATING REPRESATION INPUTS
        representation_inputs = extract_representation_inputs( representation_type = representation_type, 
                                                               representation_inputs = representation_inputs )
    
    ## FINDING PICKLE NAME AND OUTPUTTING
    pickle_name = main_output_instance_name(representation_type = representation_type,
                                            representation_inputs = representation_inputs,
                                            solute_list = solute_list,
                                            mass_frac_data = mass_frac_data,
                                            data_type = data_type,
                                            path_output = path_output)
    

    
    