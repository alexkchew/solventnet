# -*- coding: utf-8 -*-
"""
plotting_spatial_correlations.py
The purpose of this code is to load the SolventNet model and the instances, then 
generate spatial correlation maps that could give insight into what the 3D CNN 
is doing. This code was designed for Shengli (Bruce) Jiang to make maps. 

Created on: 02/10/2020

"""
## IMPORTING OS COMMANDS
import os

## TRAINING TOOLS
from train_deep_cnn import split_train_test_set

## ANALYSIS TOOLS
from analyze_deep_cnn import find_avg_std_predictions, create_dataframe

## PICKLE FUNCTIONS
from extraction_scripts import load_pickle_general

## LOADING THE MODELS
from keras.models import load_model

## LOADING NOMENCLATURE
from core.nomenclature import read_combined_name, extract_sampling_inputs, extract_instance_names

## PLOTTING PARITY PLOT
from read_extract_deep_cnn import plot_parity_publication_single_solvent_system


#%%

## MAIN FUNCTION
if __name__ == "__main__":
    ## DEFINING PATH TO SIMULATION FOLDER
    sim_folder=r"R:\scratch\3d_cnn_project\simulations"
    
    ## DEFINING SPECIFIC SIM
    specific_sim = r"20200210-SolventNet_For_Bruce"
    
    ## DEFINING SOLVENT NET SIM FOLDER
    training_folder=r"20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-strlearn-0.80-solvent_net-500-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-10_25_50_75-DIO_GVL_THF"
    
    ## DEFINING INSTANCES FILE
    instances_file = r"20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-DIO_GVL_THF-10_25_50_75"
    
    ## DEFINING PATHS
    path_to_sim = os.path.join(sim_folder,
                               specific_sim)
    path_training_folder = os.path.join(path_to_sim,
                                        training_folder)
    
    ## DEFINING PATH TO INSTANCES
    path_instances = os.path.join(path_to_sim,
                                  instances_file)
    
    ## DEFINING THE MODELS 
    model_list = [
            'model_fold_0.hdf5',
            'model_fold_1.hdf5',
            'model_fold_2.hdf5',
            'model_fold_3.hdf5',
            'model_fold_4.hdf5',
            ]
    
    #%% LOADING ALL INSTANCES
    instances = load_pickle_general(path_instances)
    '''
    instances outputs everything of shape 3:
        Index 0: voxel representations in 76 x 10 x (20 x 20 x 20 x 3)
            where each reactant/water has 10 partitions (i.e. 10 voxel representations)
            The last 2 voxel representations are used for testing
        Index 1: Experimental sigma values in with length of 76, e.g.
            [1.15,
             0.84,
             0.21,
             0.05,
             1.6,
             0.72,
             ...
             ]
            
        Index 2: Labels for each, e.g.
            ['CEL_403.15_DIO_10',
             'CEL_403.15_DIO_25',
             'CEL_403.15_DIO_50',
             'CEL_403.15_DIO_75',
             ...
             ]
            Where 'CEL_403.15_DIO_10' means cellobiose simulated at 403.15 K, with 10 wt% water / 90 wt% dioxane
    '''
    
    #%% LOADING THE TRAINED MODELS
    
    ## DEFINING SPECIFIC MODEL
    model_index = 0
    
    ## LOADING SPECIFIC MODEL INDEX
    path_model = os.path.join(path_training_folder,
                              model_list[model_index])
    
    ## LOADING MODEL
    model = load_model(path_model)
    ## PRINT MODEL SUMMARY
    model.summary()
    '''
    This should output the summary of solvent net, e.g.
        Layer (type)                 Output Shape              Param #   
        =================================================================
        input_1 (InputLayer)         (None, 20, 20, 20, 3)     0         
        _________________________________________________________________
        conv3d_1 (Conv3D)            (None, 18, 18, 18, 8)     656       
        _________________________________________________________________
        conv3d_2 (Conv3D)            (None, 16, 16, 16, 16)    3472      
        _________________________________________________________________
        ...
        dense_3 (Dense)              (None, 128)               16512     
        _________________________________________________________________
        dropout_3 (Dropout)          (None, 128)               0         
        _________________________________________________________________
        dense_4 (Dense)              (None, 1)                 129       
        =================================================================
        Total params: 172,417
        Trainable params: 172,289
        Non-trainable params: 128
        _________________________________________________________________
        
    '''
    
    #%% PREDICTING WITH THE TRAINED MODEL
    
    ## GETTING NAME INFORMATION
    combined_name_info = read_combined_name(training_folder)
    '''
    Outputs a dictionary with the training information
        {'data_type': '20_20_20_20ns_oxy_3chan',            # 20 x 20 x 20 channel with oxygen as a reactant channel
         'representation_type': 'split_avg_nonorm',         # split trajectory
         'representation_inputs': '10',                     # split trajectory 10 ways (for a 20 ns simulation)
         'sampling_type': 'strlearn',                       # Use stratified learning to divide training and test set
         'sampling_inputs': '0.80',                         # Divide training and test set by 80%
         'cnn_type': 'solvent_net',                         # SolventNet was used
         'epochs': '500',                                   # 500 epoches was trained for
         'solute_list': ['CEL', 'ETBE', 'FRU', 'LGA', 'PDO', 'XYL', 'tBuOH'], # solutes that were within training set
         'mass_frac_data': ['10', '25', '50', '75'],                          # mass fractions that were withing training set  
         'solvent_list': ['DIO', 'GVL', 'THF'],                               # solvents that were in training set
         'want_descriptor': False}                                            # whether descriptors were added to the last layer of SolventNet
    '''
    
    ## GENERATING SAMPLING DICT
    sampling_dict = extract_sampling_inputs( sampling_type = combined_name_info['sampling_type'], 
                                             sampling_inputs = [ combined_name_info['sampling_inputs'] ])
    
    ## GETTTING TRAINING AND TESTING DATA
    x_train, x_test, y_train, y_test = split_train_test_set( sampling_dict = sampling_dict,
                                                             x_data = instances[0],
                                                             y_label = instances[1])
    
    #%%
    ## PREDICTIONS
    y_pred = model.predict(x_test).reshape(len(y_test) )
    ## OUTPUTS PREDICTIONS FOR 76 * 2 voxels = 156 TOTAL PREDICTIONS
    
    ## DEFINING INSTANCE NAMES
    instance_names = instances[2]
    ''' List of instance names, e.g.
        ['CEL_403.15_DIO_10',
         'CEL_403.15_DIO_25',
         'CEL_403.15_DIO_50',
         ...
         ]
    '''
    
    ## GETTING ERROR BARS
    y_pred_avg, y_pred_std, y_true_split = find_avg_std_predictions(instance_names = instance_names,
                                                                    y_pred = y_pred,
                                                                    y_true = y_test)
    
    ## GETTING INSTANCE DICTIONARY
    instance_dict = [ extract_instance_names(name = name) for name in instance_names ]
    ''' Generates dictionary with instances
        [{'solute': 'CEL', 'temp': '403.15', 'cosolvent': 'DIO', 'mass_frac': '10'},
         {'solute': 'CEL', 'temp': '403.15', 'cosolvent': 'DIO', 'mass_frac': '25'}]
    '''
    
    ## GENERATING DATAFRAME
    dataframe = create_dataframe(instance_dict = instance_dict,
                                 y_true = y_true_split,
                                 y_pred = y_pred_avg,
                                 y_pred_std = y_pred_std)
    
    ## DEFINING FIGURE SIZE
    figure_size=( 18.542/2, 18.542/2 ) # in cm
    
    ## PLOTTING PARITY
    plot_parity_publication_single_solvent_system( dataframe = dataframe,
                                                   fig_name =  os.path.join(path_to_sim, "solventnet_model_%d_performance.png"%(model_index) ),
                                                   mass_frac_water_label = 'mass_frac',
                                                   sigma_act_label = 'y_true',
                                                   sigma_pred_label = 'y_pred',
                                                   sigma_pred_err_label = 'y_pred_std',
                                                   fig_extension = 'png',
                                                   save_fig_size = figure_size,
                                                   save_fig = True)
    
    
    
    