#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
predict_test_set.py

The purpose of this script is to predict the test set by first generating 
the test set (if necessary) and then making the predictions. 


Created on: 05/22/2020

Author(s):
    - Alex K. Chew (alexkchew@gmail.com)
    
The algorithm is as follows:
    - Input the simulation you are interested in. We will extract details about the
     simulations like the input representation, sampling, etc. as a way of 
     selecting the type of database to use.
     - Then, given the name, and any additional inputs, generate a new name 
     for the pickle file that will be used to generate testing information
     - Use the sampling, etc. and combining_arrays to generate the pickle
     - Output the pickle if it has not been written before
     

For bash script -- generating test sets
function move_test_set () {
    name=$1
    
    test_set="${name}-TESTSET"
    ## CREATING TEST SET
    mkdir "${test_set}"
    
    ## MOVING FILES
    mv -v "${name}"_{ACE,ACN,dmso}/* "${test_set}"
    mv -v "${name}"_{ACE,ACN,dmso} dump

}

"""
import os
import numpy as np
import pandas as pd


## IMPORTING NOMENCLATURE
from core.nomenclature import read_combined_name, extract_representation_inputs, extract_instance_names, extract_sampling_inputs

## IMPORTING GLOBAL VARS
from core.global_vars import PATH_MAIN_PROJECT as MAIN_PATH

## IMPORTING COMBINING ARRAYS
from combining_arrays import combine_instances

## METHOD TO LOAD TRAIN MODELS
from prediction_post_training import predict_with_trained_model

## DEFINING CLASS PATH
class_file_path =os.path.join(MAIN_PATH,"database",
                              "Experimental_Data",
                              "solvent_effects_regression_data_testset.csv")

## DEFINING NOUTPUT PATH
output_path = os.path.join(MAIN_PATH,"test_set_data")

## FINDING DATABASE


### FUNCTION TO GET TEST SET PICKLE NAME
def get_test_set_pickle_name(name_info,
                             num_partitions=None):
    '''
    This function gets the test set pickle name.
    INPUTS:
        name_info: [dict]
            name information extracted from the name using "read_combined_name
        num_partitions: [int]
            number of partitions. If None, it will use all partitions.
    OUTPUTS:
        test_set_pickle_name: [str]
            test set pickle name
    '''
    ## DEFINING PICKLE NAME
    test_set_pickle_name = '-'.join([name_info['data_type'],
                                     name_info['representation_type'],
                                     name_info['representation_inputs'],
                                     str(num_partitions)
                                     ]
                                     )
    return test_set_pickle_name

### FUNCTION TO LOAD TEST SET INSTANCES
def load_test_set_instances(sim_basename,
                            num_partitions = 2):
    '''
    The purpose of this function is to load the test set instances. 
    INPUTS:
        sim_basename: [str]
            simulation basename
        num_partitions: [int]
            number of partitions
    OUTPUTS:
        instances: [obj]
            instances object using "combine_instances"
        name_info: [dict]
            name dictionary
    '''
    
    ## EXTRACTING INFORMATION ABOUT THE SIMULATION
    name_info = read_combined_name(sim_basename)
    
    ## DEFINING DATABASE PATH
    database_path= os.path.join(MAIN_PATH,"database", 
                                "%s-TESTSET"%(name_info['data_type']))
    
    ## DEFINING PICKLE NAME
    test_set_pickle_name = get_test_set_pickle_name(name_info,
                                                    num_partitions=num_partitions)

    ## GETTING REP INPUTS
    representation_inputs = extract_representation_inputs(representation_type = name_info['representation_type'], 
                                                          representation_inputs = [name_info['representation_inputs']])

    ## COMBINING INSTANCES
    instances = combine_instances(
                     solute_list = None,
                     representation_type = name_info['representation_type'],
                     representation_inputs = representation_inputs,
                     verbose = True,
                     database_path = database_path,
                     class_file_path = class_file_path,
                     combined_database_path = output_path,
                     data_type = name_info['data_type'],
                     enable_pickle = True, # True if you want pickle on
                     output_path = None,
                     pickle_name = test_set_pickle_name,
                     skip_loading = False,
                     load_based_on_csv = True, # LOADING BASED ON THE CSV FILE
                     num_partitions = num_partitions,
                     )
    
    return instances, name_info
    

### FUNCTION TO GENERATE DATAFRAME AND PREDICT USING THE INSTANCES
def predict_test_set_with_trained_model(trained_model,
                                        test_set_instances,
                                        ):
    '''
    This function simply predicts the test set using the trained model. 
    The idea is that you insert a set of instances, then use the trained model 
    to predict an input variable. Note that we will average the prediction 
    across n models if there are more than one model. 
    INPUTS:
        trained_model: [obj]
            trained model that you are interested in using
        test_set_instances: [obj]
            instances object that contains all test set information
    OUTPUTS:
        df: [dataframe]
            dataframe containing predicted values from the model and the 
            actual true values extracted from the instances object
    '''

    ## GETTING INSTANCE DICTIONARY
    instance_dict = [ extract_instance_names(name = name) for name in test_set_instances.instance_names ]
    
    ## LOOPING AND MAKING PREDICTIONS
    x_data = np.array(test_set_instances.x_data)
    y_data = np.array(test_set_instances.y_label)
    
    ## CREATING DATAFRAME
    df = pd.DataFrame(instance_dict)
    
    ## LOOPING 
    for each_index in range(x_data.shape[1]):
        ## DEFINING THE X-DATA
        x = x_data[:, each_index]
        ## MAKING THE PREDICTIONS AND RESHAPING IT TO BE: NUM_INSTANCES X N MODELS
        y_pred = np.array([ each_model.predict(x).reshape(len(y_data)) for each_model in trained_model.model ]).T
        ## ADDING THE PREDICTIONS
        df['y_pred_%d'%(each_index)] = np.mean(y_pred, axis = 1)
    
    ## DEFINING DATAFRAMES THAT HAVE PRED
    pred_y_values = [ np.array(df['y_pred_%d'%(each_index)]) for each_index in range(x_data.shape[1])]
    
    ## NOW FINDING THE AVG AND STANDARD DEVIATIONS
    df['y_pred'] = np.mean(pred_y_values, axis = 0)
    df['y_pred_std'] = np.std(pred_y_values, axis = 0)

    ## STORING Y DATA
    df['y_act'] = y_data[:]    
    
    return df

### MAIN FUNCTION TO PREDICT THE TEST SET
def main_predict_test_set(path_to_sim,
                          num_partitions = 2,
                          model_weights_list = [ 
                                  "model_fold_0.hdf5",
                                  "model_fold_1.hdf5",
                                  "model_fold_2.hdf5",
                                  "model_fold_3.hdf5",
                                  "model_fold_4.hdf5",
                                    ],
                         verbose = True):
    '''
    The purpose of this function is to predict the test set given a path to 
    a simulation. The idea is that we use its data representation and find 
    the corresponding TESTSET information. We then use that test set information 
    and generate an instance array (which uses the splitting that was originally 
    performed on the training set.) You can specify the number of partitions 
    as a way of denoting the amount of testing set you want to do. For instance, 
    num_partition = 1 means that you will only use one partition from your 
    split trajectory to make the predictions. 
    INPUTS:
        path_to_sim: [str]
            path to the simulation
        num_partitions: [int]
            number of partitions to use. By default, we use 2 partitions. 
        model_weights_list: [list]
            list of weights to load from the path to sims
        verbose: [logical]
            True if you want to print out the details
    OUTPUTS:
        df: [dataframe]
            dataframe from the predicted test set
        trained_model: [obj]
            trained model that has all the model parameters
        test_set_instances: [obj]
            test set instances object
    '''

    ## GETTING BASENAME 
    sim_basename = os.path.basename(path_to_sim)
    ## LOADING INSTANCES
    test_set_instances, name_info = load_test_set_instances(sim_basename = sim_basename,
                                                            num_partitions = num_partitions)
    
    ## DEFINING FULL PATH TO MODEL
    path_model = [ os.path.join(path_to_sim, model_weights) for model_weights in model_weights_list ] 
    
    ## DEFINING INPUTS FOR PREDICTED MODEL
    inputs_predicted_model = {
            'path_model': path_model,
            'verbose': verbose,
            }
    ## LOADING MODEL
    trained_model = predict_with_trained_model( **inputs_predicted_model )
    
    ## PREDICTING TEST SET USING THE TRAINED MODEL
    df = predict_test_set_with_trained_model(trained_model = trained_model,
                                             test_set_instances = test_set_instances,
                                             )
    
    return df, trained_model, test_set_instances

#%%
## MAIN FUNCTION
if __name__ == "__main__":
    ## DEFINING PATH TO SIMULATION
    path_to_sim="/Volumes/akchew/scratch/3d_cnn_project/simulations/MANUSCRIPT_0_TRAINING/20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-strlearn-1.00-solvent_net-500-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-10_25_50_75-DIO_GVL_THF"
    
    ## RUNNING PREDICTION OF TEST SET
    df, trained_model, test_set_instances = main_predict_test_set(path_to_sim = path_to_sim,
                                                                  num_partitions = 2,
                                                                  model_weights_list = [ 
                                                                          "model_fold_0.hdf5",
                                                                          "model_fold_1.hdf5",
                                                                          "model_fold_2.hdf5",
                                                                          "model_fold_3.hdf5",
                                                                          "model_fold_4.hdf5",
                                                                            ],
                                                                 verbose = True)
    
    
    #%%
    ''' METHOD TO PLOT THE INFORMATION 
    from read_extract_deep_cnn import plot_parity_publication_single_solvent_system, generate_dataframe_slope_rmse
    ## PLOTTING TESET SET TOGETHER
    fig, ax = plot_parity_publication_single_solvent_system( dataframe = df,
                                                             #  fig_name = os.path.join(path_image_dir, fig_name) + '.' + fig_extension,
                                                               mass_frac_water_label = 'mass_frac',
                                                               sigma_act_label = 'y_act',
                                                               sigma_pred_label = 'y_pred',
                                                               sigma_pred_err_label = 'y_pred_std',
                                                              #  fig_extension = fig_extension,
                                                              #  save_fig_size = figure_size,
                                                               save_fig = False)
    '''
