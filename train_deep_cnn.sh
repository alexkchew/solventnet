#!/bin/bash
# train_deep_cnn.sh
# This code runs the combining array python script

# Written by: Alex K. Chew (04/22/2019)

## USAGE EXAMPLE: bash train_deep_cnn.sh

## DEFINING PYTHON SCRIPT
python_script="train_deep_cnn.py"

## DEFINING REPRESENTATION TYPE
representation_types="split_avg_nonorm" # split_avg_nonorm split_average
representation_inputs="5" # Number of splits

## DEFINING MASS FRACTION
mass_frac="10,25,50,75" # 75
cosolvents="DIO" # DIO,GVL,THF
solutes="PDO,XYL,LGA,ETBE,CEL,tBuOH,FRU"

## DEFINING MODEL
cnn_type="voxnet" # solvent_net, orion, voxnet
epochs="500"
data_type="20_20_20"

## DEFINING PATHS
database="/c/Users/akchew/Box Sync/2019_Spring/CS760/Spring_2019_CS760_Project/Datasets/20_20_20_Gridded_Data"
csv_data="/c/Users/akchew/Box Sync/2019_Spring/CS760/Spring_2019_CS760_Project/Datasets/Experimental_Data/solvent_effects_regression_data.csv"
combined_database_path="/c/Users/akchew/Box Sync/2019_Spring/CS760/Spring_2019_CS760_Project/Combined_dataset"
output_path="/c/Users/akchew/Box Sync/2019_Spring/CS760/Spring_2019_CS760_Project/Output"

database=None
csv_data=None
combined_database_path=None
output_path=None

## RUNNING PYTHON SCRIPT
python ${python_script} -v -s "${solutes}" -x "${cosolvents}" -m "${mass_frac}" -r "${representation_types}" -g "${representation_inputs}" \
						-d "${database}" -c "${csv_data}" -a "${combined_database_path}" -o "${output_path}" -q "${cnn_type}" -n "${epochs}" -z "${data_type}"
