# -*- coding: utf-8 -*-
"""
visualize_importance_maps.py
The purpose of this function is to visualize importance sampling

Written by: Alex K. Chew (03/12/2020)
Special thanks to Bruce Jiang for his development on the integrated_gradients


"""
## IMPORTING MODULES
import os
import numpy as np

## TAKING EXTRACTION SCRIPTS
from extraction_scripts import load_pickle,load_pickle_general

## IMPORTING PLOTTING SCRIPTS
import core.plotting_scripts as plotter

## IMPORTING PLOTTING TOOLS
import matplotlib.pyplot as plt

## IMPORTING COMBINING ARRAYS
from combining_arrays import combine_instances

## IMPORTING INTEGRATED GRADIENTS
from integrated_gradients import integrated_gradients

## METHOD TO LOAD TRAIN MODELS
from prediction_post_training import predict_with_trained_model

### FUNCTION TO PLOT RGB DATA
def plot_importance_data_xy_contours(importance_data,
                                     lim = [0, 20],
                                     levels = [0.10, 0.25, 0.50, 0.75, 0.90, 1.00],
                                     inc = 5,
                                     fig_size_cm = (16.8, 16.8),
                                     ):
    '''
    The purpose of this function is to plot the xy contours for an importance 
    voxel representation. Note that we renormalize each of the channels 
    for the comparison of each portion. 
    INPUTS:
        importance_data: [np.array]
            numpy array in 20, 20, 20, 3 shape
        lim: [list, shape = 2]
            limits of your image
        levels: [list]
            list of contour levels to plot
        inc: [int]
            increments between the axis
        fig_size_cm: [tuple]
            figure in cm
    OUTPUTS:
        figs, axs: 
            figures and axis of each plot
    '''
    ## GETTING AVERAGE ACROSS Z
    avg_across_z = np.mean(importance_data,axis = 2)
    
    ## NORMALIZING
    avg_across_z = plotter.renormalize_rgb_array(avg_across_z)
    
    ## DEFINING X AXIS
    x_range = np.arange(avg_across_z.shape[0])
    y_range = np.arange(avg_across_z.shape[1])
    
    ## GETTING MESHGRID
    X, Y = np.meshgrid(x_range, y_range)
    
    ## DEFINING RANGE
    lim_range = np.arange(lim[0], lim[-1] + inc , inc)
    
    ## GETTING NUMBER OF DIMENSIONS
    num_dimension=importance_data.shape[-1]
    
    ## STORING FIGS AND AX
    figs = []
    axs = []
    
    ## LOOOPING
    for dimension in range(num_dimension):    
    
        ## GETTING EACH DIMENSION AND SHOWING EACH ONE
        if dimension == 0:
            cmap = 'Reds'
        elif dimension == 1:
            cmap = 'Greens'
        elif dimension == 2:
            cmap = 'Blues'
            
        ## DEFINING Z DIMENSION
        Z = avg_across_z[...,dimension]
        
        ## PLOTTING
        fig, ax = plotter.create_fig_based_on_cm(fig_size_cm = fig_size_cm)
        
        ## ADDING AXIS
        ax.set_xlabel('x')
        ax.set_ylabel('y')

        ## SETTING LIMITS
        ax.set_xlim(lim)
        ax.set_ylim(lim)
        
        ## SETTING XTICKS
        ax.set_xticks(lim_range)
        ax.set_yticks(lim_range)
        
        ## ADDING GRID
        ax.grid()
        
    
        ## PLOTTING
        cs = ax.contourf(X, Y, Z,
                         levels = levels,
                         cmap = cmap)
        
        ## ADDING LEGEND
        proxy = [ plt.Rectangle((0,0),1,1, fc = pc.get_facecolor()[0]) for pc in cs.collections]
        ax.legend(proxy, [str(each_value) for each_value in levels])
        
        ## APPENDING
        figs.append(fig)
        axs.append(axs)
    return figs, axs

### FUNCTION TO PLOT IMPORTANCE IMAGING
def plot_importance_voxels(importance_data,
                           voxel_cutoff= 0.10,
                           alpha=0.15,
                           figsize = [8, 6],
                           want_separate_axis= True,
                           want_renormalize= True,
                           **inputs):
    '''
    The purpose of this function is to plot the importance voxels.
    INPUTS:
        importance_data: [np.array]
            numpy array in 20, 20, 20, 3 shape
        voxel_cutoff: [float]
            voxel to cutoff and prevent from plotting. 
        alpha: [float]
            transparency level for the split image
        figsize: [list, shape=2]
            figure size
        want_separate_axis: [logical]
            True/False if you want axis to be separated
        want_renormalize: [logical]
            True if you want to renormalize your image
    OUTPUTS:
        fig, ax:
            figure and axis of the importance voxels
    '''
    

    ## PLOTTING INPUTS
    plot_voxel_split_inputs = {
            'alpha': alpha,
            'want_renormalize': want_renormalize,
            # 'tick_limits': np.arange(0, 32+4, 8), # np.append(, 31), # np.append(np.arange(0, 31, 4), 31), # np.arange(0, 33, 4) , 
            'figsize': figsize,
            'want_separate_axis': want_separate_axis,
            'voxel_cutoff': voxel_cutoff, # 0.15
            **inputs
            }
    
    ## PLOTTING
    fig, ax = plotter.plot_voxel_split(importance_data, **plot_voxel_split_inputs)
    
    ## MAKING TIGHT LAYOUT
    if type(fig) is list:
        [current_fig.tight_layout() for current_fig in fig]
    else:
        fig.tight_layout()
    
    return fig, ax

### FUNCTION TO READ INSTANCE NAMES
def read_instance_name_from_txt(path_instances_names):
    '''
    This function simply reads the instance name from the text file.
    '''
    with open(path_instances_names, "r") as f:
        instance_file_name = f.readline()
    return instance_file_name

### FUNCTION TO LOAD INSTANCES FROM A PATH SIM
def load_instances_within_sim(path_to_sim,
                              instances_names_txt = "instance_name.txt"):
    '''
    The purpose of this function is to load instances that are saved 
    next to the train model.
    INPUTS:
        path_to_sim: [str]
            path to simulation
        instances_names_txt: [str]
            text file that stored the instances names
    OUTPUTS:
        instances: [obj]
            instances object
    '''
    ## DEFINING PATH TO INSTANCES
    path_instances_names = os.path.join(path_to_sim,
                                        instances_names_txt)
    

    
    
    ## GETTING INSTANCE FILE NAME
    instance_file_name = read_instance_name_from_txt(path_instances_names = path_instances_names)
    
    ## LOADING THE DATA
    instances = combine_instances(
                     output_path = path_to_sim,
                     pickle_name = instance_file_name,
                     )
    
    return instances

### CLASS OBJECT THAT GENERATES THE IMPORTANCE MAPS
class generate_importance_maps:
    '''
    This class holds functions to generate importance maps.
    INPUTS:
        void
    OUTPUTS:
        
    FUNCTIONS:
        load_model:
            function that loads the model
        load_instances:
            function that loads the instances within the simulation
        compute_importance_data:
            function that generates the importance data
        
    '''
    def __init__(self):
        
        return
    
    ## LOADING THE FUNCTION
    @staticmethod
    def load_model(path_to_sim,
                   model_weights_list = ["model.hdf5"],
                   verbose = True):
        '''
        This function simply loads a trained model.
        INPUTS:
            path_to_sim: [str]
                path to the simulation
            model_weights_list: [list]
                model weights in a form of a list
            verbose: [logical]
                True if you want to print loading of models
        OUTPUTS:
            trained_model: [obj]
                trained model object that you can access the model using "trained_model.model"
        '''
        ## DEFINING FULL PATH TO MODEL
        path_model = [ os.path.join(path_to_sim, model_weights) for model_weights in model_weights_list ] 
        
        ## DEFINING INPUTS FOR PREDICTED MODEL
        inputs_predicted_model = {
                'path_model': path_model,
                'verbose': verbose,
                }
        ## LOADING MODEL
        trained_model = predict_with_trained_model( **inputs_predicted_model )
        return trained_model
    
    ## FUNCTION THAT LOADS THE INSTANCES
    @staticmethod
    def load_instances(**args):
        '''
        This function loads the instances within sims
        INPUTS:
            path_to_sim: [str]
                path to simulation
            instances_names_txt: [str]
                name of the instances text file that has the instance pickle name
        OUTPUTS:
            instances: [obj]
                object that has th instances
        '''
        ## LOADING THE INSTANCES
        instances = load_instances_within_sim(**args)
        
        return instances
    
    ### FUNCTION TO COMPUTE IMPORTANCE DATA
    @staticmethod
    def compute_importance_data(instances,
                                trained_model,
                                model_idx = 0,
                                desired_system = "XYL_403.15_DIO_10",
                                partition_idx = 0):
        '''
        The purpose of this function is to compute importance data given 
        the set of instances and a trained model.
        INPUTS:
            instances: [obj]
                instances object
            trained_model: [obj]
                trained model object
            model_idx: [int]
                index of the trained model that you want. If you only have one 
                model, then keep this as 0.
            desired_system: [str]
                desired system based on instance.instance_names
            partition_idx: [int]
                partition that you want the importance data from, e.g. 0 means 
                that you want the first voxel representation from the instance.x_data
        OUTPUTS:
            importance_data: [np.array, shape=(20,20,20,3) or x_data.shape from instance.x_data]
                importance data
        '''
        ## FINDING INDEX
        instance_idx = instances.instance_names.index(desired_system)
        
        ## GETTING INSTANCE INFO
        x = instances.x_data[instance_idx][partition_idx]
        model = trained_model.model[model_idx]
        
        ## COMPUTING INTEGRATED GRADIENTS
        ig = integrated_gradients(model)
        ref = np.zeros(x.shape) # (20, 20, 20, 3)
        exp = ig.explain(x, reference=ref, outc=0)
        importance_data = np.abs(exp)
        
        return importance_data


#%%
## MAIN FUNCTION
if __name__ == "__main__":
    
    ## RUNNING GENERATIVE FUNCTION
    importance_obj = generate_importance_maps()
    
    ## DEFINING PATH TO SIMULATION
    path_to_sim="/Volumes/akchew/scratch/3d_cnn_project/simulations/MANUSCRIPT_0_TRAINING_3DCNNS_ALLDATA/20_20_20_20ns_oxy_3chan-split_avg_nonorm-10-strlearn-1.00-solvent_net-500-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-10_25_50_75-DIO_GVL_THF"
    
    ## LOADING THE MODEL
    trained_model = importance_obj.load_model(path_to_sim,
                                              model_weights_list = ["model.hdf5"],
                                              verbose = True)
    
    ## LOADING INSTANCES
    instances = importance_obj.load_instances(path_to_sim = path_to_sim)
    
    ## COMPUTING IMPORTANCE DATA
    importance_data = importance_obj.compute_importance_data(instances = instances,
                                                             trained_model = trained_model,
                                                             model_idx = 0,
                                                             desired_system = "XYL_403.15_DIO_10",
                                                             partition_idx = 0)
    

    #%%

    ## PLOTTING IMPORTANCE VOXELS
    figs, axs = plot_importance_voxels(importance_data,
                                     voxel_cutoff= 0.10,
                                     alpha=0.15,
                                     figsize = [8, 6],
                                     want_separate_axis= False,
                                     want_renormalize= True)
    
    #%%
    
    ## PLOTTING IMPORTANCE VOXELS
    figs, axs = plot_importance_voxels(importance_data,
                                     voxel_cutoff= 0.10,
                                     alpha=0.15,
                                     figsize = [8, 6],
                                     want_separate_axis= True,
                                     want_renormalize= True)
    
    #%%
    
    ## PLOTTING IMPORTANCE MAPS
    figs, axs = plot_importance_data_xy_contours(importance_data)


    '''
    ## DEFINING PATH TO MAIN PATH
    path_importance_sampling = r'R:\scratch\3d_cnn_project\importance_sampling'
    
    ## DEFINING IMPORTANT pickles
    pickle_name = r'XYL_403.15_DIO_10_importance.pickle'
    
    ## LOADING PICKLE FILE
    path_pickle = os.path.join(path_importance_sampling,pickle_name)
    
    ## LOADING THE DATA
    importance_data = load_pickle_general(path_pickle)
    '''
    # Grid shape: 20 x 20 x 20 x 3
    '''
    
    ## DEFINING FIGURE SIZE
    figsize = [8, 6]
    
    '''